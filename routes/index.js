// var express = require('express');
// var router = express.Router();
var mysql = require("mysql");
// var http      =     require('http').Server(express);
// var io        =     require("socket.io")(http);

var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var axios = require('axios');
var querystring = require('querystring');

server.listen(8081);

/* Creating POOL MySQL connection.*/

/* local Host */
/*var pool    =    mysql.createPool({
  connectionLimit   :   100,
  host              :   'localhost',
  user              :   'root',
  password          :   '',
  database          :   'hrtime_db',
  debug             :   false
});*/

/* HRTIME Host */
var pool = mysql.createPool({
    connectionLimit: 0,
    queueLimit : 0,
    multipleStatements : true,
    host: '172.29.1.75',
    user: 'HRTIME',
    password: '*HRTIME*',
    database: 'HRTIME_DB',
    debug: false
});

var tranidLast = 0
var tranidCheck = 0
var DataToShow = []
var Cookies = ''

/* GET home page. */
// router.get('/', function(req, res, next) {
//   // res.render('index', { title: 'Express' });
// });

app.get('/', function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.sendFile(__dirname + '/testIo.html');
    setInterval(function () {
        try{
            /*getData().then(function (res) {
                SendToShow(res).then(function (value) {
                    console.log('success')
                })
            });*/
        } catch (e) {
            throw e;
        }
    }, 100)
    // res.send({
    //   status: true
    // })
});

io.on('connection', function (socket) {
    console.log("A user is connected");
    setInterval(function () {
        try{
            /*getData().then(function (res) {
                SendToShow(res).then(function (value) {
                    console.log('success send Web Procress => ' + new Date().toLocaleString())
                })
            });*/

            /*getDataFormCS().then(function (res) {
                if (res.data.status) {
                    if(res.data.data) {
                        res.data.data.map(function (value, index) {
                            getDataFormDB(value).then(function (res) {
                                if(res.data.status && res.data.data.length > 0) {
                                    var param = querystring.stringify(
                                        {
                                            message: value.data[6].substring(value.data[6].indexOf('(') + 1,value.data[6].indexOf(')')) + '(' + res.data.data[0].PERID + ')' + ' หน่วยงาน : ' + res.data.data[0].Dep_name + ' ตำแหน่ง : ' + res.data.data[0].PosName +  ' สแกนบัตร : ' + value.data[0] + ' พื้นที่ : ' + value.data[1]  + ' ประตู : ' + value.data[3] + '',
                                            Token: 'eQDlEEnpTF1OoeNMU8hZ8ThOBVCafVqtxo2r1ZACbA2'
                                        }
                                    )

                                    io.emit('refresh feed', {
                                        ...res.data.data[0],
                                        event_time: value.data[0],
                                        area_name: value.data[1],
                                        door_name: value.data[3]
                                    });
                                }
                            }, function (reject) {
                                // console.log(reject)
                            })
                        })
                    }

                }
            }, function (reject) {

            })*/
        } catch (e) {
            throw e;
        }
    }, 100)
    socket.on('Request Notify', function (data) {
        LineNoti(data, function (res) {
            if (res) {
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear() + 543;
                var hour = today.getHours();
                var minute = today.getMinutes();
                var second = today.getSeconds();
                if (hour.toString().length == 1) {
                    hour = '0' + hour;
                }
                if (minute.toString().length == 1) {
                    minute = '0' + minute;
                }
                if (second.toString().length == 1) {
                    second = '0' + second;
                }
                today = dd + '/' + mm + '/' + yyyy + ' ' + hour + ':' + minute + ':' + second

                // axios.get('https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/cardnew/line_noti/').then(res => {
                //
                // })
                var param = querystring.stringify(
                    {
                        message: data.msg,
                        Token: data.Line_Token
                    }
                )

                axios.post('https://medhr.medicine.psu.ac.th/sendLineNotify.php', param)
            } else {
                io.emit('error');
            }
        })
        /*add_status(status,function(res){
          if(res){
            io.emit('refresh feed',status);
          } else {
            io.emit('error');
          }
        });*/
    });
});

function getData() {
    return new Promise(function (resolutionFunc, rejectionFunc){
        DataToShow = []
        getTimeEx = "SELECT\n" +
            "A.trans_id,\n" +
            "DATE_FORMAT(A.event_time, '%d-%m-%Y %H:%i:%s') AS event_time,\n" +
            "A.PERID,\n" +
            "A.door_id,\n" +
            "A.door_name,\n" +
            "A.event_id,\n" +
            "B.CARD_NO,\n" +
            "B.dept_id,\n" +
            "C.`NAME`,\n" +
            "C.SURNAME,\n" +
            "D.Dep_name,\n" +
            "D.Dep_Group_name,\n" +
            "E.door_name,\n" +
            "E.zone_id\n" +
            "FROM\n" +
            "HRTIME_DB.D_transaction AS A\n" +
            "LEFT JOIN HRTIME_DB.C_card AS B ON B.PERID = A.PERID\n" +
            "LEFT JOIN STAFF.Medperson AS C ON C.PERID = A.PERID\n" +
            "LEFT JOIN STAFF.Depart AS D ON D.Dep_Code = C.DEP_WORK\n" +
            "LEFT JOIN HRTIME_DB.C_door AS E ON E.door_id = A.door_id\n" +
            "WHERE\n" +
            "A.PERID != '0' AND\n" +
            "C.CSTATUS != '0' AND\n" +
            "E.zone_id != '5' AND\n" +
            "E.zone_id != '9' AND\n" +
            "A.event_time BETWEEN '" + getDateNow() + " 00:00:00' AND '" + getDateNow() + " 23:59:59'\n" +
            "ORDER BY trans_id ASC"
        pool.getConnection(function (err, connection) {
            try{
                if(connection) {
                    connection.query(getTimeEx, function (err, rows) {
                        connection.release();
                        if (!err) {
                            resolutionFunc(rows)
                        } else {
                            rejectionFunc(err)
                        }
                    });
                }
            } catch (e) {
                console.log(e)
                console.log('error time => '  + new Date().toLocaleString())
                // throw e
            } finally {

            }
        })
    });
}

function SendToShow(rows) {
    return new Promise(function (resolutionFunc, rejectionFunc){
        if (tranidLast != 0) {
            // tranidLast = 39632
            if(rows.length > 0) {
                if (tranidLast < rows[rows.length - 1].trans_id) {
                    tranidCheck = tranidLast
                    tranidLast = rows[rows.length - 1].trans_id
                    rows.map((item) => {
                        if(item.trans_id > tranidCheck
                )
                    {
                        DataToShow.push(item)
                    }
                })
                    // tranidLast = rows[rows.length - 1].trans_id
                    io.emit('refresh feed', DataToShow);
                }
                resolutionFunc(true)
            } else {
                rejectionFunc(false)
            }
        } else {
            tranidLast = rows[rows.length - 1].trans_id
            // tranidLast = 6371942
        }
    })
}

var LineNoti = function (status, callback) {
    callback(true);
}

var add_status = function (status, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(false);
            return;
        }
        connection.query("INSERT INTO `C_Test_Table` (`C_Test`) VALUES ('" + status + "')", function (err, rows) {
            connection.release();
            if (!err) {
                callback(true);
            }
        });
        connection.on('error', function (err) {
            callback(false);
            return;
        });
    });
}

var getDateNow = function () {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var hour = today.getHours();
    var minute = today.getMinutes();
    var second = today.getSeconds();
    if (hour.toString().length == 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        second = '0' + second;
    }
    return yyyy + '-' + mm + '-' + dd
}

function getDataFormDB(item) {
    return new Promise(function (resolutionFunc, rejectionFunc){
        DataToShow = []
        getTimeEx = "SELECT\n" +
            "\tA.`NAME`,\n" +
            "\tA.SURNAME,\n" +
            "\tA.PERID,\n" +
            "\tB.Dep_name,\n" +
            "\tB.Dep_Group_name,\n" +
            "\tC.PosName\n" +
            "FROM\n" +
            "\tSTAFF.Medperson AS A\n" +
            "INNER JOIN HRTIME_DB.C_card AS D ON A.PERID = D.PERID\n" +
            "INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK\n" +
            "INNER JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos\n" +
            "WHERE\n" +
            "\tD.CARD_NO = " + item.data[5]
        pool.getConnection(function (err, connection) {
            try{
                if(connection) {
                    connection.query(getTimeEx, function (err, rows) {
                        connection.release();
                        if (!err) {
                            resolutionFunc({data: {status:true,data: rows}})

                        } else {
                            rejectionFunc({data: {status:false,data: err}})
                        }
                    });
                }
            } catch (e) {
                console.log(e)
                console.log('error time => '  + new Date().toLocaleString())
            } finally {

            }
        })
    });
}

function getDataFormCS() {
    return new Promise(function (resolutionFunc, rejectionFunc) {
        var header = {
            headers: {
                Cookie: Cookies,
                withCredentials: true
            }
        }
        try {
            axios.get('http://172.29.154.252:8088/accRTMonitorAction!getEventData.action?clientId=eventData1590465825952&systemCode=acc',header).then(function(res) {
                if(res.status == 201)
                {
                    rejectionFunc({data: {status:false,data: null}})
                    getCookies().then(function(res) {
                        if(res.data.status)
                        {
                            Cookies = res.data.data
                            resolutionFunc({data: {status:true,data: null}})
                        } else {
                            rejectionFunc({data: {status:false,data: null}})
                        }
                    })
                }
                else
                {
                    resolutionFunc({data: {status:true,data:res.data.rows}})
                }
            })
        } catch (e) {
            console.log(e)
        } finally {

        }
    })
}

function getCookies() {
    return new Promise(function (resolutionFunc, rejectionFunc) {
        axios.post('http://172.29.154.252:8088/authLoginAction!login.do?username=chaiyan&password=chaiyan2020', '').then(function(res) {
            if(res.data.ret == 'ok') {
                resolutionFunc({data: {status:true,data:res.headers['set-cookie'][0] + ':' + res.headers['set-cookie'][1] + ':' + res.headers['set-cookie'][2]}})
            } else {
                rejectionFunc({data: {status:false,data: null}})
            }
        })
    })
}

module.exports = app;
